package app

import (
	"fmt"
	tmtypes "github.com/tendermint/abci/types"
	"github.com/tendermint/go-merkle"
	dbm "github.com/tendermint/go-db"
	"encoding/hex"
	"gitlab.com/peerity/chaincore/service"
	"github.com/tendermint/tendermint/node"
	//"reflect"
	//"gitlab.com/peerity/chaincore/types"
	//"strings"
	//"encoding/json"
	//"github.com/tendermint/tendermint/rpc/core"
	"gitlab.com/peerity/chaincore/ui"

	"log"
)


type PeerityApp struct {
	Db   		dbm.DB
	State 		merkle.Tree
	Height 		uint64
	TxCount   	uint64			// dont need this!!!!?
}

type PeerityVars struct {
	App 		*PeerityApp
	Node 		*node.Node
	ValsChange 	[]*tmtypes.Validator 	// validator set
}

// global var
var PeerityGlobal = PeerityVars{}

//var current_BlockHeader *types.Header
var savedAppHash []byte = []byte("savedAppHash")
var hasTx bool		// indicate current block has any TX



func NewPeerityApp() *PeerityApp {

	db := dbm.NewDB("state", dbm.GoLevelDBBackendStr, ".")
	state := merkle.NewIAVLTree(0, db)

	lastBlock := service.LoadLastBlock(db)
	state.Load(lastBlock.AppHash)
	savedAppHash = state.Hash()

	ui.Log("New Peerity App", "block", lastBlock.Height, "root: ", hex.EncodeToString(state.Hash()))

	return &PeerityApp{ Db: db, State: state, Height: lastBlock.Height, TxCount: lastBlock.TxCount}
}

func (app *PeerityApp) Info() (resInfo tmtypes.ResponseInfo) {
	resInfo = tmtypes.ResponseInfo{}
	resInfo.Data = "PeerityApp"

	lastBlock := service.LoadLastBlock(app.Db)
	resInfo.LastBlockHeight = lastBlock.Height
	resInfo.LastBlockAppHash = lastBlock.AppHash
	ui.Log("Info", "block", lastBlock.Height, "root", hex.EncodeToString(lastBlock.AppHash))
	ui.UpdateBlockHeight(lastBlock.Height);
	return resInfo
}

func (app *PeerityApp) SetOption(key string, value string) (logstr string) {
	ui.Log("PeerityApp.SetOption", key, value)
	hasTx = true
	return Exec_SetOption(key, value)
}

func (app *PeerityApp) DeliverTx(tx []byte) tmtypes.Result {
	//ui.Log("PeerityApp.DeliverTx")
	hasTx = true
	return Exec_DeliverTx(tx)
}

func (app *PeerityApp) CheckTx(tx []byte) tmtypes.Result {
	//ui.Log("PeerityApp.CheckTx")
	return Exec_CheckTx(tx)
}

func (app *PeerityApp) Commit() tmtypes.Result {
	//ui.Log("PeerityApp.Commit")

	// dont need to save state if there is no transaction or state doesnt change
	if hasTx || (app.Height <= 2)  { // (app.Height % 10000 == 0
		savedAppHash = app.State.Save()
		lastBlock := service.LastBlockInfo {
			Height:  app.Height,
			AppHash: savedAppHash, 			// this hash will be in the next block header
			TxCount: app.TxCount,
		}

		service.SaveLastBlock(app.Db, lastBlock)
		ui.UpdateLastBlock();
	}

	return tmtypes.NewResultOK(savedAppHash, "")
}

func (app *PeerityApp) Query(query []byte) tmtypes.Result {
	ui.Log("PeerityApp.Query")
	return tmtypes.NewResultOK(nil, fmt.Sprintf("Query is not supported"))
}

func (app *PeerityApp) InitChain(vals []*tmtypes.Validator) {
	ui.Log("PeerityApp.InitChain")
}

func (app *PeerityApp) BeginBlock(hash []byte, header *tmtypes.Header) {
	ui.Log("PeerityApp.BeginBlock ", " height = ", header.Height, " hash = ", hex.EncodeToString(hash))
	
	/////////////////////////////////////////////////
	app.Height = header.Height
	//CloneValue(header, &current_BlockHeader)
	//current_BlockHeader = header
	hasTx = false		// enter new block, reset value


	/////////////////////////////////////////////////
	// block reward
	BlockReward()
}

func (app *PeerityApp) EndBlock(height uint64) (resEndBlock tmtypes.ResponseEndBlock) {
	ui.Log("PeerityApp.EndBlock ", "height = ", height, "size = ", app.State.Size())
	ui.UpdateBlockHeight(height)

	// clone validator set change
	ValsChangeClone := make([]*tmtypes.Validator, len(PeerityGlobal.ValsChange))

	if (len(PeerityGlobal.ValsChange) > 0) {
		log.Println("Applying ValsChange")
		copy(ValsChangeClone, PeerityGlobal.ValsChange)

		PeerityGlobal.ValsChange = make([]*tmtypes.Validator, 0) // reset valset changes
	}

	return tmtypes.ResponseEndBlock{Diffs: ValsChangeClone}
}


// need to figure out a way to use username for validator without forking tendermint
func BlockReward() {
	//// check if the proposer is the current private validator on this node
	//if (PeerityGlobal.Node != nil &&
	//	(PeerityGlobal.Node.ConsensusState() != nil) &&
	//	((uint64(PeerityGlobal.Node.ConsensusState().Height) == PeerityGlobal.App.Height) &&
	//	(PeerityGlobal.Node.ConsensusState().Validators != nil))) {
	//
	//	//update UI if running.
	//	ui.UpdateNodeCount(PeerityGlobal.Node.ConsensusState().Validators.Size());
	//
	//	proposer_address := PeerityGlobal.Node.ConsensusState().Validators.Proposer().
	//	if (reflect.DeepEqual(proposer_address, PeerityGlobal.Node.PrivValidator().Address)) {
	//
	//		ui.Log("this node validator is the proposer of the current block")
	//
	//		// OK, execute and broadcast BlockProposerRewardOperation
	//		var tx types.Transaction
	//		tx.Fee = 0
	//		tx.Pubkey = PeerityGlobal.Node.PrivValidator().PubKey.KeyString()
	//		ui.Log("tx.Pubkey=", tx.Pubkey)
	//
	//		tx.Type = "BlockProposerRewardOperation"
	//		opt := types.BlockProposerRewardOperation{
	//			Username: hex.EncodeToString(proposer_address), // address of the validator
	//			Height:        PeerityGlobal.App.Height, // which block
	//			Amount: 1, // how much, default 1 token
	//		}
	//
	//		opt_arr, err := json.Marshal(opt)
	//		if (err != nil) {
	//			ui.Log(err.Error())
	//			return
	//		}
	//		//ui.Log("opt:", string(opt_arr))
	//		opt_str := strings.ToUpper(hex.EncodeToString(opt_arr))
	//
	//		tx.Operation = opt_str
	//
	//		// signature
	//		sign := PeerityGlobal.Node.PrivValidator().PrivKey.Sign([]byte(opt_str))
	//		sign_str := strings.ToUpper(hex.EncodeToString(sign.Bytes()))
	//		sign_str = sign_str[2:len(sign_str)]
	//
	//		tx.Signature = sign_str
	//
	//		// encode to json
	//		json_bytes, _ := json.Marshal(tx)
	//
	//		//ui.Log("tx=", string(json_bytes))
	//
	//		str_hex := hex.EncodeToString(json_bytes)
	//
	//		//service.TM_broadcast_tx_commit(json_bytes)
	//		core.BroadcastTxAsync([]byte(str_hex))
	//	}
	//
	//}
}