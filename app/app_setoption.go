package app

import (
	"encoding/hex"
	"log"
	"strings"
	"strconv"
	//"gitlab.com/peerity/chaincore/db"
	"gitlab.com/peerity/chaincore/types"
	"gitlab.com/peerity/chaincore/service"
)

/**
glogChainApp.SetOption("genesis.block/create.account", "pool/449F6F39391BC9E918CE51DB10F6FAADF65077263E715B17652425CD7827C814/1000000")
 */
func Exec_SetOption(key string, value string) (logstr string) {
	var err error

	switch key {
	case "genesis.block/create.account":
		strs := strings.Split(value, "/")
		if (len(strs) != 3) {
			return "Invalid input data"
		}

		var account types.Account

		account.Username = strs[0]
		account.PubKey, err = hex.DecodeString(strs[1])
		if (err != nil) {
			log.Println(err.Error())
			return err.Error()
		}
		account.Sequence = 1
		account.Balance, err = strconv.ParseInt(strs[2], 10, 64)
		if err != nil {
			return err.Error()
		}

		//account.Groups = [][]byte{}

		//
		//pubkey, err := types.GetPubKeyFromBytes(account.PubKey)
		//if err != nil {
		//	log.Println(err.Error())
		//	return err.Error()
		//}


		// Save to Merkle Tree App State
		_, err = service.TreeGetAccount(PeerityGlobal.App.State, account.Username)
		if (err == nil) { // return if foud
			return err.Error()
			//return
		}

		err = service.TreeSaveAccount(PeerityGlobal.App.State, account)
		if (err != nil) {
			log.Println(err.Error())
			return err.Error()
		}

		break
	case "genesis.block/token.transfer":
		break
	default:
	}

	return ""
}