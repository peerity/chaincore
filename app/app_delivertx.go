package app

import (
	tm_types "github.com/tendermint/abci/types"
	"log"
	"encoding/hex"
	"bytes"
	"encoding/gob"
	"gitlab.com/peerity/chaincore/types"
	"gitlab.com/peerity/chaincore/service"
	"errors"
	"fmt"
	"encoding/json"
	"strings"
)

/**
tx is json string, need to convert to text and then parse into json object
 */
func Exec_DeliverTx(tx []byte) tm_types.Result {
	var err error

	// decode jsonstring to OperationEnvelope and Operation
	transaction, err := types.HexStringToTransaction(string(tx[:]))
	if (err != nil) {
		log.Println(err.Error())
		return tm_types.ErrEncodingError
	}

	operation, err := transaction.ParseOperation()
	if (err != nil) {
		log.Println(err.Error())
		return tm_types.ErrEncodingError
	}

	/////////////
	//bPubKey, err := hex.DecodeString(transaction.Pubkey)
	//if (err != nil) {
	//	log.Println(err.Error())
	//	return tm_types.ErrEncodingError
	//}

	//PubKey, err := types.GetPubKeyFromBytes(bPubKey)
	//if (err != nil) {
	//	log.Println(err.Error())
	//	return tm_types.ErrInternalError
	//}
	//
	//Address := PubKey.Address() // address of the user who make the transaction

	var buf bytes.Buffer        // Stand-in for a network connection
	enc := gob.NewEncoder(&buf) // Will write to network.
	dec := gob.NewDecoder(&buf) // Will read from network.

	err = enc.Encode(operation)
	if (err != nil) {
		log.Println(err.Error())
		return tm_types.ErrInternalError
	}

	switch operation.(type) {
	case types.AccountCreateOperation:
		var accountCreateOperation types.AccountCreateOperation
		err = dec.Decode(&accountCreateOperation)
		if err != nil {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}

		// store to state
		var account types.Account
		account.PubKey, err = hex.DecodeString(accountCreateOperation.Pubkey)
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}
		//copy(account.PubKey, barr)

		account.Username = strings.ToLower(accountCreateOperation.Username)

		account.Sequence = 1
		account.Balance = 0

		// add 100 free tokens from thin air if on testnet
		if (types.PeerityConfigGlobal.TestNet) {
			account.Balance = 100
		}

		err = service.TreeSaveAccount(PeerityGlobal.App.State, account)
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}
		break
	case types.SendTokenOperation:
		var cmdSendToken service.SendTokenStateCommand
		cmdSendToken.From = strings.ToLower(transaction.Username)

		opt, ok := operation.(types.SendTokenOperation)
		if (!ok) {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}

		cmdSendToken.To = strings.ToLower(opt.ToUsername)
		cmdSendToken.Amount = opt.Amount

		err = service.Exec_SendToken(PeerityGlobal.App.State, cmdSendToken)
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrEncodingError
		}

		break
	case types.GroupCreateOperation:
		groupCreateOperation, ok := operation.(types.GroupCreateOperation)
		if (!ok) {
			log.Println("Can not cast operation to GroupCreateOperation")
			return tm_types.ErrInternalError
		}

		err = service.Exec_CreateGroup(PeerityGlobal.App.State, groupCreateOperation)
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrEncodingError
		}

		break
	case types.GroupJoinRequestOperation:
		groupJoinRequestOperation, ok := operation.(types.GroupJoinRequestOperation)
		if (!ok) {
			log.Println("Can not cast operation to GroupJoinRequestOperation")
			return tm_types.ErrInternalError
		}

		// check if this group existing or not
		grp, err := service.TreeGetGroup(PeerityGlobal.App.State, groupJoinRequestOperation.GroupId)
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}

		groupJoinPendingRequest := types.GroupJoinPendingRequest {
				Requestor: 	transaction.Username,
				Message: 	groupJoinRequestOperation.Message }
		grp.Requests = append(grp.Requests, groupJoinPendingRequest)

		service.TreeSaveGroup(PeerityGlobal.App.State, grp)

		break
	case types.PostCreateOperation:
		postCreateOperation, ok := operation.(types.PostCreateOperation)
		if (!ok) {
			log.Println("Can not cast operation to PostCreateOperation")
			return tm_types.ErrInternalError
		}

		post := types.Post {
			Title: postCreateOperation.Title,
			Content: postCreateOperation.Content,
			Author: transaction.Username,
			Group: postCreateOperation.Group,
			Image: postCreateOperation.Image,
			Cat: postCreateOperation.Cat,
			Location: postCreateOperation.Location,
		}

		service.TreeSavePost(PeerityGlobal.App.State, post)

		break
	case types.CommentCreateOperation:
		commentCreateOperation, ok := operation.(types.CommentCreateOperation)
		if (!ok) {
			log.Println("Can not cast operation to CommentCreateOperation")
			return tm_types.ErrInternalError
		}

		comment := types.Comment {
			PostID: commentCreateOperation.PostID,
			Parent: commentCreateOperation.ParentComment,
			Author: transaction.Username,
			Content: commentCreateOperation.Content,
			Nounce: commentCreateOperation.Nounce,
		}

		service.TreeSaveComment(PeerityGlobal.App.State, comment)

		break
	case types.BlockProposerRewardOperation:
		blockProposerRewardOperation, ok := operation.(types.BlockProposerRewardOperation)
		if (!ok) {
			log.Println("Can not cast operation to BlockProposerRewardOperation")
			return tm_types.ErrInternalError
		}

		//validatorAddress, err := hex.DecodeString(blockProposerRewardOperation.Address)
		//if (err != nil) {
		//	log.Println(err.Error())
		//	return tm_types.ErrEncodingError
		//}

		validatorAccount, err := service.TreeGetAccount(PeerityGlobal.App.State, blockProposerRewardOperation.Username)
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}

		validatorAccount.Balance += int64(blockProposerRewardOperation.Amount)
		validatorAccount.Sequence ++

		service.TreeSaveAccount(PeerityGlobal.App.State, validatorAccount)

		break
	case types.ProposalCreateOperation:
		proposalCreateOperation, ok := operation.(types.ProposalCreateOperation)
		if (!ok) {
			log.Println("Can not cast operation to ProposalCreateOperation")
			return tm_types.ErrInternalError
		}

		// TODO: FIX THIS - MUST NOT USE DYNAMIC VALUES TO AVOID PANIC
		// get the current validator set
		//voteState := make([]types.VoteState, 0)
		//
		//if (PeerityGlobal.Node != nil &&
		//	(PeerityGlobal.Node.ConsensusState() != nil) &&
		//	((uint64(PeerityGlobal.Node.ConsensusState().Height) == PeerityGlobal.App.Height) &&
		//		(PeerityGlobal.Node.ConsensusState().Validators != nil))) {
		//
		//	for _, v := range PeerityGlobal.Node.ConsensusState().Validators.Validators {
		//
		//		val := types.VoteState {
		//			Addr: v.Address,
		//			Power: uint64(v.VotingPower),
		//			Point: -128, // -128: unknown - not vote, 0: no, 1: vote for both or nothing, 2: yes
		//		}
		//
		//		voteState = append(voteState, val)
		//	}
		//}
		//
		//log.Println("voteState=", voteState)

		proposal := types.Proposal {
			Type: proposalCreateOperation.Type,
			Title: proposalCreateOperation.Title,
			Content: proposalCreateOperation.Content,
			Proposer: transaction.Username,
			Data: proposalCreateOperation.Data,
			Height: proposalCreateOperation.Height,
			Votes:	proposalCreateOperation.Votes,
		}

		service.TreeSaveProposal(PeerityGlobal.App.State, proposal) // save to tree state

		break
	case types.ProposalVoteOperation:
		proposalVoteOperation, ok := operation.(types.ProposalVoteOperation)
		if (!ok) {
			log.Println("Can not cast operation to ProposalVoteOperation")
			return tm_types.ErrInternalError
		}

		// check if proposal exist or not
		proposal_ID_bytes, err := hex.DecodeString(proposalVoteOperation.ProposalKey)
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}

		proposal, err := service.TreeGetProposal(PeerityGlobal.App.State, proposal_ID_bytes)
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}

		voteState, err :=  proposal.GetVoteState(transaction.Username)
		if (err != nil) {
			// voter is not in validator set ==> no permission
			err = errors.New("Not Found Voter username in allowed list") // no permission
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}

		// check if voted then not allow to vote again
		if (voteState.Point != -128) {
			err = errors.New("Can vote once only")
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}

		proposal.UpdateVoteState(transaction.Username, proposalVoteOperation.Vote)

		service.TreeSaveProposal(PeerityGlobal.App.State, proposal) // save to tree state

		////////////////////////////////////////////////////////////////
		// check if enough votes then move to waiting queue for executing at target height
		if (proposal.IsVotesPassThreshold()) {
			proposal_arr, err := hex.DecodeString(proposal.Data)
			if (err != nil) {
				log.Println(err.Error())
				return tm_types.ErrInternalError
			}

			switch proposal.Type {
			case "ProposalDataValidatorSetChange":
				var proposalDataValidatorSetChange types.ProposalDataValidatorSetChange
				if err = json.Unmarshal(proposal_arr, &proposalDataValidatorSetChange); err != nil {
					log.Println(err.Error())
					return tm_types.ErrInternalError
				}

				for _, v := range proposalDataValidatorSetChange.Diffs {
					log.Println("PubKey", v.PubKey, "Power", v.Power)

					bPubKey, err := hex.DecodeString(v.PubKey)
					if (err != nil) {
						log.Println(err.Error())
						return tm_types.ErrInternalError
					}

					pubKey, err := types.GetPubKeyFromBytes(bPubKey)
					if (err != nil) {
						log.Println(err.Error())
						return tm_types.ErrInternalError
					}

					// NOTE: expects go-wire encoded pubkey
					val_change := &tm_types.Validator {
						PubKey: pubKey.Bytes(),
						Power: v.Power,
					}


					// TODO: need to validate if username of new validator existed or not
					//address := pubKey.Address() // address of validator
					_, err = service.TreeGetAccount(PeerityGlobal.App.State, transaction.Username)
					if (err != nil) {
						log.Println("Can not get validator username - ", err.Error())
						return tm_types.ErrInternalError
					}

					// all good!
					PeerityGlobal.ValsChange = append(PeerityGlobal.ValsChange, val_change)
				}

				break
			default:
				err = fmt.Errorf("unknown proposal.Type")
				log.Println(err.Error())
				return tm_types.ErrInternalError
			}
		}

		break
	default:
		log.Println("Unknow Operation")
	}

	PeerityGlobal.App.TxCount++
	//return types.NewResult(types.CodeType_OK, tx, "DeliverTx OK")
	return tm_types.OK
}