package app

import (
	tm_types "github.com/tendermint/abci/types"
	"encoding/hex"
	"log"
	"gitlab.com/peerity/chaincore/service"
	"gitlab.com/peerity/chaincore/types"
	"bytes"
	"reflect"
	"strings"
)

/**
 * should be very long validating...
 */
func Exec_CheckTx(tx []byte) tm_types.Result {
	var err error

	//log.Println("tx", string(tx[:]))

	// decode jsonstring to OperationEnvelope and Operation
	transaction, err := types.HexStringToTransaction(string(tx[:]))
	if (err != nil) {
		log.Println(err.Error())
		return tm_types.ErrEncodingError
	}

	log.Println("transaction.Operation", transaction.Operation)

	operation, err := transaction.ParseOperation()
	if (err != nil) {
		log.Println(err.Error())
		return tm_types.ErrEncodingError
	}

	///////////////////////////////////
	// Validate the envelope
	///////////

	// decode PubKey
	bPubKey, err := hex.DecodeString(transaction.Pubkey)
	if (err != nil) {
		log.Println(err.Error())
		return tm_types.ErrEncodingError
	}

	pubKey, err := types.GetPubKeyFromBytes(bPubKey)
	if (err != nil) {
		log.Println(err.Error())
		return tm_types.ErrInternalError
	}

	// decode SignatureEd25519
	bSignature, err := hex.DecodeString(transaction.Signature)
	if (err != nil) {
		log.Println(err.Error())
		return tm_types.ErrEncodingError
	}

	signature, err := types.GetSignatureFromBytes(bSignature)
	if (err != nil) {
		log.Println(err.Error())
		return tm_types.ErrInternalError
	}

	// verify signature
	verify := pubKey.VerifyBytes([]byte(transaction.Operation), signature)
	if (!verify) {
		log.Println("Can not verify signature")
		return tm_types.ErrUnauthorized
	}

	// check if username existing
	if (transaction.Type != "AccountCreateOperation") {
		_, err = service.TreeGetAccount(PeerityGlobal.App.State, strings.ToLower(transaction.Username))
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}
	}

	// validate the Fee
	if (transaction.Fee < 0) {
		log.Println("Fee value is invalid")
		return tm_types.ErrInternalError
	}

	///////////////////////////////////
	// Validate the operation
	///////////

	switch operation.(type) {
	case types.AccountCreateOperation:
		_, ok := operation.(types.AccountCreateOperation)
		if (!ok) {
			log.Println("Can not cast operation to AccountCreateOperation")
			return tm_types.ErrInternalError
		}

		break
	case types.SendTokenOperation:
		_, ok := operation.(types.SendTokenOperation)
		if (!ok) {
			log.Println("Can not cast operation to SendTokenOperation")
			return tm_types.ErrInternalError
		}

		break
	case types.GroupCreateOperation:
		groupCreateOperation, ok := operation.(types.GroupCreateOperation)
		if (!ok) {
			log.Println("Can not cast operation to GroupCreateOperation")
			return tm_types.ErrInternalError
		}

		// check if Group unique
		group := types.Group {
			Name: groupCreateOperation.Name,
			Description: groupCreateOperation.Description,
			Owner: groupCreateOperation.Owner }

		_, _, exists := PeerityGlobal.App.State.Get(group.GroupKey())
		if (exists) {
			log.Println("Group is not unique")
			return tm_types.ErrInternalError
		}

		// check owner
		if strings.Compare(transaction.Username, group.Owner) != 0 {
			log.Println("Invalid Owner")
			return tm_types.ErrInternalError
		}

		break
	case types.GroupJoinRequestOperation:
		groupJoinRequestOperation, ok := operation.(types.GroupJoinRequestOperation)
		if (!ok) {
			log.Println("Can not cast operation to GroupJoinRequestOperation")
			return tm_types.ErrInternalError
		}

		// check if this group existing or not
		grp, err := service.TreeGetGroup(PeerityGlobal.App.State, groupJoinRequestOperation.GroupId)
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}

		// check if belong to this group or not
		acc, err := service.TreeGetAccount(PeerityGlobal.App.State, transaction.Username)
		if (err != nil) {
			log.Println(err.Error())
			return tm_types.ErrInternalError
		}

		for _, groupId := range acc.Groups {
			if bytes.Equal(groupId, groupJoinRequestOperation.GroupId) {
				log.Println("You already in this group")
				return tm_types.ErrInternalError
			}
		}

		// check for dupplicated pending requests
		for _, pendingRequest := range grp.Requests {
			if strings.Compare(pendingRequest.Requestor, transaction.Username) == 0 {
				log.Println("There is a pending request")
				return tm_types.ErrInternalError
			}
		}

		break
	case types.PostCreateOperation:
		postCreateOperation, ok := operation.(types.PostCreateOperation)
		if (!ok) {
			log.Println("Can not cast operation to PostCreateOperation")
			return tm_types.ErrInternalError
		}

		// check if post unique
		post := types.Post {
			Title: postCreateOperation.Title,
			Content: postCreateOperation.Content,
			Author: transaction.Username,
			Group: postCreateOperation.Group,
			Image: postCreateOperation.Image,
			Cat: postCreateOperation.Cat,
			Location: postCreateOperation.Location,
		 }

		_, _, exists := PeerityGlobal.App.State.Get(post.PostKey())
		if (exists) {
			log.Println("Post is not unique")
			return tm_types.ErrInternalError
		}

		break
	case types.CommentCreateOperation:
		commentCreateOperation, ok := operation.(types.CommentCreateOperation)
		if (!ok) {
			log.Println("Can not cast operation to CommentCreateOperation")
			return tm_types.ErrInternalError
		}

		// 1. validate PostID if exist
		_, err := service.TreeGetPost(PeerityGlobal.App.State, commentCreateOperation.PostID)
		if (err != nil) {
			log.Println("No Post found!")
			return tm_types.ErrInternalError
		}

		// 2. validate Parent Comment if exist
		if (commentCreateOperation.ParentComment != nil) {
			if len(commentCreateOperation.ParentComment) > 0 {
				_, err := service.TreeGetPost(PeerityGlobal.App.State, commentCreateOperation.PostID)
				if (err != nil) {
					log.Println("No Post found!")
					return tm_types.ErrInternalError
				}
			}
		}

		// 3. content length must be min 2 characters
		if len(commentCreateOperation.Content) < 2 {
			log.Println("Invalid Content")
			return tm_types.ErrInternalError
		}

		// 4. validate Comment if exist base on caculated Comment ID
		comment := types.Comment {
			PostID: commentCreateOperation.PostID,
			Parent: commentCreateOperation.ParentComment,
			Author: transaction.Username,
			Content: commentCreateOperation.Content,
			Nounce: commentCreateOperation.Nounce,
		}

		_, _, exists := PeerityGlobal.App.State.Get(comment.CommentKey())
		if (exists) {
			log.Println("Comment is not unique")
			return tm_types.ErrInternalError
		}

		break
	case types.BlockProposerRewardOperation:
		log.Println("Broken BlockProposerRewardOperation when using username instead of address!")
		return tm_types.ErrUnknownRequest

		//blockProposerRewardOperation, ok := operation.(types.BlockProposerRewardOperation)
		//if (!ok) {
		//	log.Println("Can not cast operation to BlockProposerRewardOperation")
		//	return tm_types.ErrInternalError
		//}
		//
		////validatorUsername, err := hex.DecodeString(blockProposerRewardOperation.Username)
		////if (err != nil) {
		////	log.Println(err.Error())
		////	return tm_types.ErrEncodingError
		////}
		//
		//_, err = service.TreeGetAccount(PeerityGlobal.App.State, blockProposerRewardOperation.Username)
		//if (err != nil) {
		//	log.Println(err.Error())
		//	return tm_types.ErrInternalError
		//}
		//
		//if (blockProposerRewardOperation.Height == PeerityGlobal.App.Height  ) {
		//	if (!validate_current_proposer(blockProposerRewardOperation.Username)) {
		//		log.Println("proposer does not match with ConsensusState")
		//		return tm_types.ErrInternalError
		//	}
		//}
		//
		//if (blockProposerRewardOperation.Amount != 1) { // block reward =1 only for testnet
		//	log.Println("blockProposerRewardOperation.Amount must be 1")
		//	return tm_types.ErrInternalError
		//}

		break
	case types.ProposalCreateOperation:
		proposalCreateOperation, ok := operation.(types.ProposalCreateOperation)
		if (!ok) {
			log.Println("Can not cast operation to ProposalCreateOperation")
			return tm_types.ErrInternalError
		}

		//timeNow := time.Now().Unix()

		// check if proposal unique
		proposal := types.Proposal {
			Type: proposalCreateOperation.Type,
			Title: proposalCreateOperation.Title,
			Content: proposalCreateOperation.Content,
			Proposer: transaction.Username,
			//CreatedDate: timeNow,		// unixtime
			Data: proposalCreateOperation.Data,
			Height: proposalCreateOperation.Height,
			Votes: proposalCreateOperation.Votes, // eg., the current validator set
		}

		_, _, exists := PeerityGlobal.App.State.Get(proposal.ProposalKey())
		if (exists) {
			log.Println("Proposal is not unique")
			return tm_types.ErrInternalError
		}

		///////////////////////////
		// TODO: more validation <====== very inportance on production chain.
		// - title: length, special characters...
		// - Type: in list of supported types
		// - Content: length, format
		// - Proposer: is the validator
		// - Data: valid data or not
		// - Height: min of current height + 360 ~ 1 hour

		break
	case types.ProposalVoteOperation:

		log.Println("Broken ProposalVoteOperation when using username instead of address!")
		return tm_types.ErrUnknownRequest


		//proposalVoteOperation, ok := operation.(types.ProposalVoteOperation)
		//if (!ok) {
		//	log.Println("Can not cast operation to ProposalVoteOperation")
		//	return tm_types.ErrInternalError
		//}
		//
		//// check if proposal exist or not
		//proposal_ID_bytes, err := hex.DecodeString(proposalVoteOperation.ProposalKey)
		//if (err != nil) {
		//	log.Println(err.Error())
		//	return tm_types.ErrInternalError
		//}
		//
		//proposal, err := service.TreeGetProposal(PeerityGlobal.App.State, proposal_ID_bytes)
		//if (err != nil) {
		//	log.Println(err.Error())
		//	return tm_types.ErrInternalError
		//}
		//
		//voteState, err :=  proposal.GetVoteState(transaction.Username)
		//if (err != nil) {
		//	// voter is not in validator set ==> no permission
		//	err = errors.New("Not Found Voter address in allowed list") // no permission
		//	log.Println(err.Error())
		//	return tm_types.ErrInternalError
		//}
		//
		//// check if voted then not allow to vote again
		//if (voteState.Point != -128) {
		//	err = errors.New("Can vote once only")
		//	log.Println(err.Error())
		//	return tm_types.ErrInternalError
		//}

		break
	default:
		log.Println("Unknow Operation!")
		return tm_types.ErrUnknownRequest
	}

	//TODO: more validation here AND avoid double spending within current block

	return tm_types.OK
}


func validate_current_proposer(validator_address []byte) bool {
	if (PeerityGlobal.Node == nil) {
		return false
	}

	if (PeerityGlobal.Node.ConsensusState() == nil) {
		return false
	}

	if (PeerityGlobal.Node.ConsensusState().Validators == nil) {
		return false
	}

	proposer_address := PeerityGlobal.Node.ConsensusState().Validators.Proposer().Address
	if (!reflect.DeepEqual(proposer_address, validator_address)) {
		return false
	}

	return true;
}

//func IsProposer() bool  {
//	if (PeerityGlobal.Node != nil) {
//		if (PeerityGlobal.Node.ConsensusState() != nil) {
//			if (PeerityGlobal.Node.ConsensusState().Validators != nil) {
//				proposer_address := PeerityGlobal.Node.ConsensusState().Validators.Proposer().Address
//				if (reflect.DeepEqual(proposer_address, PeerityGlobal.Node.PrivValidator().Address)) {
//					return true
//				}
//			}
//		}
//	}
//
//	return false
//}