package types

import (
	"golang.org/x/crypto/ripemd160"
	"encoding/binary"
)

type Comment struct {
	PostID                 	[]byte 		// which post belong to
	Parent 			[]byte 		// parrent comment, empty string means no parent
	Author          	string		// Username
	Content         	string
	Nounce			int64		// any number like block heigh or unixtime or sequence number to avoid duplicated comment
}

func (comment *Comment) CommentKey() []byte {
	hasher := ripemd160.New()
	hasher.Write([]byte(comment.PostID))
	hasher.Write([]byte(comment.Parent))
	hasher.Write([]byte(comment.Author))

	// http://stackoverflow.com/questions/35371385/how-can-i-convert-an-int64-into-a-byte-array-in-go
	Sequence_bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(Sequence_bytes, uint64(comment.Nounce))
	hasher.Write(Sequence_bytes)

	hash := hasher.Sum(nil)

	return CommentKey(hash)
}

func CommentKey(hash []byte) []byte {
	// ref from tm: return append([]byte("base/a/"), addr...)
	return append([]byte("peerity/c/"), hash...)
}
