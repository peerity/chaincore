package types

import (
	//"log"
)


/**
username rules:
- 4 to 16 chars length
- case-insensitive, contains a-z, 0-9, and [-_.] only
- must start with a character a-z
 */

// account to store on merkle tree
type Account struct {
	Username 	string
	PubKey   	[]byte 		//[32]byte , crypto.PubKeyEd25519
	Sequence 	int64
	Balance  	int64
	Groups 		[][]byte 	// array of group ID
	//Inbox 		[][]byte	// all messages
}

func (acc *Account) AccountKey() []byte {
	//pubkey, err := GetPubKeyFromBytes(acc.PubKey)
	//if err != nil {
	//	log.Println(err.Error())
	//	return nil
	//}

	return AccountKey(acc.Username)
}

func AccountKey(username string) []byte {
	return append([]byte("base/a/"), []byte(username)...)
}