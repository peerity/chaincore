package types

import (
	"fmt"
	"log"

	"path/filepath"
	"io/ioutil"
	"encoding/json"
	"reflect"
	"strings"

)


type PeerityCfg struct {
	//WebRootDir 		string	// dont need anymore because using https://github.com/elazarl/go-bindata-assetfs
	//PeerchainWebAddr string
	//TmspAddr         string
	TmRpcLaddr       string
	IpFsAPI          string
	IpFsGateway      string
	TmRoot           string 	// where to store the tendermint data (chain + config for TM)
	TestNet		bool	// is testnet or not
}

type BlockchainSettings struct {
	Proxy_app 		string
	Abci 			string
	Node_laddr 		string
	Seeds 			[]string
	Fast_sync		bool
	Moniker			string
	Log_level		string
	Db_backend		string
	Rpc_laddr		string
	Block_size		uint32
	Timeout_propose		uint32
	Timeout_propose_delta	uint32
	Timeout_precommit	uint32
	Timeout_precommit_delta	uint32
	Timeout_commit		uint32
}

var PeerityConfigGlobal = PeerityCfg{}

func (b BlockchainSettings) MarshalText() ([]byte, error) {
	//bc:=BlockchainSettings{"proxy","abci", "node", []string{"ip1", "ip2"}, false, "log", "db", "rpc", 11,12,13, 14, 15, 16}
	k := reflect.TypeOf(b)
	v := reflect.ValueOf(b);
	fileData := "";

	for i := 0; i < k.NumField(); i++ {
		name := k.Field(i).Name;
		value := v.FieldByName(name).Interface();
		if( reflect.TypeOf(value) == reflect.TypeOf([]string{"",""}) ) {
			valuesArray:= value.([]string)
			valueString:="";
			if(len(valuesArray)>0) {
				valueString+=valuesArray[0];
				for i := 1; i < len(valuesArray); i ++ {
					valueString+= "," + valuesArray[i];
				}
			}
			fileData += fmt.Sprintf("%v=\"%v\"", strings.ToLower(name), valueString);
			fileData += "\n";

		} else if(reflect.TypeOf(value) == reflect.TypeOf("")) {
			fileData += fmt.Sprintf("%v=\"%v\"", strings.ToLower(name), value);
			fileData += "\n";
		} else {
		  	fileData += fmt.Sprintf("%v=%v", strings.ToLower(name), value);
			fileData += "\n";
		}
	}
	return  []byte(fileData), nil;
}

func ReadConfig() error  {
	//config := PeerityCfg{}

	filename, _ := filepath.Abs("./config.json")
	fmt.Printf("filename: %s\n", filename)

	configFile, err := ioutil.ReadFile(filename)

	if err != nil {
		panic(err)
		return err
	}

	err = json.Unmarshal(configFile, &PeerityConfigGlobal)
	if err != nil {
		log.Fatalf("error: %v", err)
		return err
	}

	//fmt.Printf("config:\n%v\n\n", config)
	//fmt.Printf("Value: %s\n", config.Title)


	return nil
}
