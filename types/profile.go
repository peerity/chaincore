package types

type Profile struct {
	PubKey   	[]byte 	//[32]byte , crypto.PubKeyEd25519
	Sequence 	int64
	Balance  	int64
				    //Username 		string
	WantTo 		[]string

}


type WantTo struct {
	Type 		string	/* start, attend, meet */
	Activity 	string
}