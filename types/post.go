package types

import (
	"golang.org/x/crypto/ripemd160"
	"strings"
)

// store on merkle tree
type Post struct {
	//ID                  	string 		// Hash of ( Author + Title ) <== should be unique
	//v int32 // version number
	Title           	string
	Content         	string
	Author          	string 		// Account Username
	Group			[]byte 		// ID of group
	//CreatedDate            	int64 		// unixtime
	//ModifiedDate        	int64 		// unixtime
	Image 		    	string 		// url to thumb image
	Cat 			[]string 	// categories
	Location		string 		// related location 
	//gpslat		int32 //GPS latitude
	//gpslong		int32 //GPS longitude
}

func (post *Post) PostKey() []byte {
	hasher := ripemd160.New()
	hasher.Write([]byte(post.Author))
	hasher.Write([]byte(strings.ToUpper(post.Title)))

	//// http://stackoverflow.com/questions/35371385/how-can-i-convert-an-int64-into-a-byte-array-in-go
	//CreatedDate_bytes := make([]byte, 8)
	//binary.LittleEndian.PutUint64(CreatedDate_bytes, uint64(post.CreatedDate))
	//hasher.Write(CreatedDate_bytes)

	hash := hasher.Sum(nil)

	return PostKey(hash)
}

func PostKey(hash []byte) []byte {
	// ref from tm: return append([]byte("base/a/"), addr...)
	return append([]byte("peerity/p/"), hash...)
}