package types

import (
	"golang.org/x/crypto/ripemd160"
	"strings"
)

// Group to store on merkle tree using key = ripemd160 hash of Uppercase(Name) - 20 bytes -- must be unique
type Group struct {
	Name		string		// must be unique, so the Key is unique
	Description 	string		// type anything here
	Owner 		string		// Username of account owner
	//Members 	[][]byte	// array of account addresses
	Requests 	[]GroupJoinPendingRequest	// pending requests: GroupJoinPendingRequest
}

type GroupJoinPendingRequest struct {
	Requestor 	string		// requestor Username
	Message 	string		// message to the group owner. Encrypt using owner pubkey????
}

func (grp *Group) GroupKey() []byte {
	hasher := ripemd160.New()
	hasher.Write([]byte(strings.ToUpper(grp.Name))) // does not error

	hash := hasher.Sum(nil)

	return GroupKey(hash)
}

func GroupKey(hash []byte) []byte {
	// ref from tm: return append([]byte("base/a/"), addr...)
	return append([]byte("peerity/g/"), hash...)
}