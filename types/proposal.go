package types

import (
	"golang.org/x/crypto/ripemd160"
	"strings"
	"reflect"
	"errors"
	"log"
)

// store on merkle tree
type Proposal struct {
	//ID                  	string 		// Hash of UpperCase ( Title ) <== should be unique

	Type 			string 		// proposal type
	Title           	string
	Content         	string
	Proposer          	string 		// username of account Proposer
	//CreatedDate            	int64 		// unixtime
	Data 			string 		// json string encoded in hex string format
	Height 			uint64		// target block height, =0 means active immediately when enough votes
	Votes			[]VoteState 	// eg., the current validator set
}

type ProposalDataValidatorSetChange struct {
	Diffs			[]ValidatorChange
}

type ValidatorChange struct {
	PubKey		string
	Power  		uint64
}


type VoteState struct {
	Username 	string 		// Username of voter
	Power  		uint64		// power of the voting time
	Point 		int8		// -128: unknown - not vote, 0: no, 1: vote for both or nothing, 2: yes
}

func (proposal *Proposal) ProposalKey() []byte {
	hasher := ripemd160.New()
	hasher.Write([]byte(strings.ToUpper(proposal.Title)))

	//// http://stackoverflow.com/questions/35371385/how-can-i-convert-an-int64-into-a-byte-array-in-go
	//CreatedDate_bytes := make([]byte, 8)
	//binary.LittleEndian.PutUint64(CreatedDate_bytes, uint64(post.CreatedDate))
	//hasher.Write(CreatedDate_bytes)

	hash := hasher.Sum(nil)

	return ProposalKey(hash)
}

func ProposalKey(hash []byte) []byte {
	// ref from tm: return append([]byte("base/a/"), addr...)
	// /g/ means governance
	return append([]byte("peerity/g/"), hash...)
}

func (proposal *Proposal) ContainsUsername(username string) (res bool)  {
	for _, v := range proposal.Votes {
		if (reflect.DeepEqual(username, v.Username)) {
			return true
		}
	}

	//err = errors.New("Not Found")
	return false
}

func (proposal *Proposal) GetVoteState(username string) (votestate VoteState, err error) {
	for _, v := range proposal.Votes {
		if (reflect.DeepEqual(username, v.Username)) {
			votestate = v
			return
		}
	}

	//votestate= nil
	err = errors.New("Not Found")
	return
}

func (proposal *Proposal) UpdateVoteState(username string, point int8) (err error) {
	// find index first

	index := -1 // <0 means not found

	for i, v := range proposal.Votes {
		if (reflect.DeepEqual(username, v.Username)) {
			//log.Println("UpdateVoteState for ", address, " with point=", address)
			//v.Point = point
			//return nil

			index = i

			break
		}
	}

	if (index < 0) {
		err = errors.New("Not Found")
		return
	}

	proposal.Votes[index].Point = point

	return nil
}

func (proposal *Proposal) IsVotesPassThreshold() (res bool) {
	point_sum := proposal.GetVotePoints()
	threshold := proposal.GetPassThreshold()

	log.Println("point_sum=", point_sum, "threshold=", threshold)

	if (point_sum >= threshold) {
		return true
	}

	return false
}

func (proposal *Proposal) GetVotePoints() int64 {
	point_sum := int64(0)

	for _, v := range proposal.Votes {
		if (v.Point != -128) {
			point_sum += int64(v.Point)
		}
	}

	return int64(point_sum)
}

func (proposal *Proposal) GetPassThreshold() int64 {
	threshold := len(proposal.Votes)

	return int64(threshold)
}