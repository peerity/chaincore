package types

import (
	//tmtypes "github.com/tendermint/abci/types"
)



////////////////////////////////////////
// Account

type AccountCreateOperation struct {
	Username 		string	// see Account struct
	Pubkey  		string
	//UserRegistered 		string
	//DisplayName 		string
}

//type AccountUpdateOperation struct {
//	// need to define here
//}


////////////////////////////////////////
// crypto currency
type SendTokenOperation struct {
	ToUsername 		string
	Amount 			int64
}


////////////////////////////////////////
// Group

type GroupCreateOperation Group

type GroupJoinRequestOperation struct {
	GroupId 		[]byte		// ripemd160 hash of Uppercase(Group Name), see types.GroupKey
	Message 		string		// message to the group owner. Encrypt using owner pubkey????
}


////////////////////////////////////////
// Post

type PostCreateOperation struct {
	Title           	string
	Content         	string
	Group			[]byte 		// ID of group
	Image 		    	string 		// url to thumb image
	Cat 			[]string 	// categories
	Location 		string 		// related location
}

//type PostEditOperation Post

//type CommentCreateOperation Comment
type CommentCreateOperation struct {
	Content         	string
	PostID                 	[]byte 		// which post belong to
	ParentComment		[]byte 		// empty string means no parent
	Nounce			int64		// any number like block heigh or unixtime or sequence number to avoid duplicated comment
}

//type VoteOperation struct {
//	PostId 		string
//	Voter 		string
//}

////////////////////////////////////////
// Reward

// Reward to Validator who is proposer of success block commit
type BlockProposerRewardOperation struct {
	Username 		string		// username of the validator
	Height 			uint64		// which block
	Amount 			uint64 		// how much, default 1 token
}



////////////////////////////////////////
// Blockchain governance

type ProposalCreateOperation struct {
	Type 			string 		// proposal type
	Title           	string
	Content         	string
	Data 			string 		// json string encoded in hex string format
	Height 			uint64		// target block height, =0 means active immediately when enough votes
	Votes 			[]VoteState	// voters must be fixed to avoid panic ( diff app hash state )
}

type ProposalVoteOperation struct {
	ProposalKey		string 		// ID - hex string hash of ProposalCreateOperation
	Vote 			int8		// 0: no, 1: vote for both or nothing, 2: yes
}
