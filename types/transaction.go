package types

import (
	"encoding/json"
	"encoding/hex"
	"strings"
	"gitlab.com/peerity/chaincore/ui"
	"fmt"
	"log"
)

// In prototype, we'll use json because we don't need high performance and protocols will need to be change however.
// use dynamic json, more at http://eagain.net/articles/go-dynamic-json/
// should look at steem operations for references
// https://github.com/steemit/steem/blob/73a2b522e609925d444acfeb40264c5a0e682705/libraries/protocol/include/steemit/protocol/operations.hpp

type Transaction struct {
	Type 		string
	Operation 	string 		// json hex string of PostCreateOperation, PostEditOperation ...
	Signature 	string 		// crypto.SignatureEd25519 to the Operation, which is in json string
	Username 	string		// use username instead of address
	Pubkey 		string 		// to verify and indentify who makes the transaction
	Fee		int64		// always 0 in test/dev pharse
}

/**
 Should be called after set the tx.Operation
 */
func (tx *Transaction) sign(hex_prikey string) error {
	prikey_bytes, _ := hex.DecodeString(hex_prikey);
	prikey, _ := GetPrivateKeyFromBytes(prikey_bytes)
	sign := prikey.Sign([]byte(tx.Operation))
	sign_str := strings.ToUpper(hex.EncodeToString(sign.Bytes()))
	sign_str = sign_str[2:len(sign_str)]

	tx.Signature = sign_str
	return nil
}

func (tx *Transaction) ParseOperation() (operation interface{}, err error) {
	opt_arr, err := hex.DecodeString(tx.Operation)
	if (err != nil) {
		log.Println(err)
		return
	}

	ui.Log("opt_arr", string(opt_arr[:]))

	switch tx.Type {
	case "AccountCreateOperation":
		var accountCreateOperation AccountCreateOperation
		if err = json.Unmarshal(opt_arr, &accountCreateOperation); err != nil {
			log.Println(err)
			return
		}
		operation = accountCreateOperation
		break
	case "SendTokenOperation":
		var sendTokenOperation SendTokenOperation
		if err = json.Unmarshal(opt_arr, &sendTokenOperation); err != nil {
			log.Println(err)
			return
		}
		operation = sendTokenOperation
		break
	case "GroupCreateOperation":
		var groupCreateOperation GroupCreateOperation
		if err = json.Unmarshal(opt_arr, &groupCreateOperation); err != nil {
			log.Println(err)
			return
		}
		operation = groupCreateOperation
		break
	case "PostCreateOperation":
		var postCreateOperation PostCreateOperation
		if err = json.Unmarshal(opt_arr, &postCreateOperation); err != nil {
			log.Println(err)
			return
		}
		operation = postCreateOperation
		break
	case "CommentCreateOperation":
		var commentCreateOperation CommentCreateOperation
		if err = json.Unmarshal(opt_arr, &commentCreateOperation); err != nil {
			log.Println(err)
			return
		}
		operation = commentCreateOperation
		break
	case "BlockProposerRewardOperation":
		var blockProposerRewardOperation BlockProposerRewardOperation
		if err = json.Unmarshal(opt_arr, &blockProposerRewardOperation); err != nil {
			log.Println(err)
			return
		}
		operation = blockProposerRewardOperation
		break
	case "ProposalCreateOperation":
		var proposalCreateOperation ProposalCreateOperation
		if err = json.Unmarshal(opt_arr, &proposalCreateOperation); err != nil {
			log.Println(err)
			return
		}
		operation = proposalCreateOperation
		break
	case "ProposalVoteOperation":
		var proposalVoteOperation ProposalVoteOperation
		if err = json.Unmarshal(opt_arr, &proposalVoteOperation); err != nil {
			log.Println(err)
			return
		}
		operation = proposalVoteOperation
		break
	default:
		err = fmt.Errorf("unknown Operation type")
		ui.Log("unknown Operation type: %q", tx.Type)
		return
	}

	//err = fmt.Errorf("Can not parse Operation")
	return
}

func UnMarshal(jsonstring string) (tx Transaction, err error) {
	ui.Log("UnMarshal", jsonstring)

	err = json.Unmarshal([]byte(jsonstring), &tx)
	if (err != nil) {
		ui.Log(err.Error())
		return
	}

	return
}

func HexStringToTransaction(hexstr string) (tx Transaction, err error) {
	// decode hex string to json string
	arr, err := hex.DecodeString(hexstr)
	if (err != nil) {
		ui.Log(err.Error())
		//return tm_types.ErrEncodingError
		return
	}
	jsonstring:= string(arr[:])

	// decode jsonstring to OperationEnvelope and Operation
	tx, err = UnMarshal(jsonstring)
	//if (err != nil) {
	//	ui.Log(err.Error())
	//	return tm_types.ErrEncodingError
	//}

	return
}