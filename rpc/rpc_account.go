package rpc

import (
	"log"
	"net/http"
	"gitlab.com/peerity/chaincore/service"
	"gitlab.com/peerity/chaincore/app"
	"encoding/hex"
	"strings"
)

type AccountService struct {

}

type AccountService_Request string // username

type AccountService_Response struct {
	PubKey   	string
	Username 	string
	Sequence 	int64
	Balance  	int64
	Groups 		[]AccountService_Response_Group
}

type AccountService_Response_Group struct {
	ID 		[]byte
	Name		string
}

func (t *AccountService) GetAccountState(r *http.Request, req *AccountService_Request, res *AccountService_Response) error {
	acc, err := service.TreeGetAccount(app.PeerityGlobal.App.State, string(*req))
	if (err != nil) {
		log.Println(err.Error())
		return err
	}

	res.PubKey = strings.ToUpper(hex.EncodeToString(acc.PubKey))
	res.Username = strings.ToLower(string(*req))
	res.Sequence = acc.Sequence
	res.Balance = acc.Balance

	for _, groupId := range acc.Groups {
		groupState, err := service.TreeGetGroup(app.PeerityGlobal.App.State, groupId)
		if (err != nil) {
			log.Println(err.Error())
			return err
		}

		accountService_Response_Group := AccountService_Response_Group {
			ID: groupId,
			Name: groupState.Name }

		res.Groups = append(res.Groups, accountService_Response_Group)
	}

	return nil
}


