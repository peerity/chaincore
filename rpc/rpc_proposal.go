package rpc

import (
	"net/http"
	"log"
	"encoding/hex"
	"strings"
	"gitlab.com/peerity/chaincore/service"
	"gitlab.com/peerity/chaincore/app"
	"gitlab.com/peerity/chaincore/types"
)

type ProposalService struct {

}

type ProposalService_Request string // ID of Proposal

type ProposalService_Response struct {
	ID			string
	types.Proposal
}

func (t *ProposalService) GetProposal(r *http.Request, req *ProposalService_Request, res *ProposalService_Response) error {
	ID := string(*req)
	log.Println("GetProposal", ID)

	ID_bytes, err := hex.DecodeString(ID)
	if (err != nil) {
		return err
	}

	proposal, err := service.TreeGetProposal(app.PeerityGlobal.App.State, ID_bytes)
	if (err != nil) {
		return err
	}

	res.ID = strings.ToUpper(ID)
	res.Proposal = proposal

	return nil
}


