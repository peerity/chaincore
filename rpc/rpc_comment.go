package rpc

import (
	"net/http"
	"log"
	"encoding/hex"
	"strings"
	"gitlab.com/peerity/chaincore/service"
	"gitlab.com/peerity/chaincore/app"
)

type CommentService struct {

}

type CommentService_Request string // ID of comment

type CommentService_Response struct {
	ID			string
	PostID			string
	Parent 			string
	Content         	string
	Author          	string
}

func (t *CommentService) GetComment(r *http.Request, req *CommentService_Request, res *CommentService_Response) error {
	ID := string(*req)
	log.Println("GetComment", ID)

	ID_bytes, err := hex.DecodeString(ID)
	if (err != nil) {
		return err
	}

	comment, err := service.TreeGetComment(app.PeerityGlobal.App.State, ID_bytes)
	if (err != nil) {
		return err
	}

	res.ID = strings.ToUpper(ID)
	res.PostID = strings.ToUpper(hex.EncodeToString(comment.PostID))
	res.Parent = strings.ToUpper(hex.EncodeToString(comment.Parent))
	res.Content = comment.Content
	res.Author = comment.Author

	return nil
}


