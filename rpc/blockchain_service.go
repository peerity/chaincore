package rpc

import (
	"log"
	"net/http"
	"gitlab.com/peerity/chaincore/types"
	"encoding/json"
	"github.com/tendermint/tendermint/rpc/core"
	"encoding/hex"
	//tmtypes "github.com/tendermint/abci/types"
	//"gitlab.com/peerity/chaincore/app"
	//"gitlab.com/peerity/chaincore/service"
)

type TransactionService struct {

}

//type TransactionService_Request struct {
//	Jsonstr 	string 		// json of types.Transaction
//}

type TransactionService_Response struct {
	Msg 	string //interface{}
}

func (t *TransactionService) Broadcast(r *http.Request, req *types.Transaction, res *TransactionService_Response) error {
	log.Println("Broadcast")
	//log.Println(req.Jsonstr)
	//log.Println("req.Type=", req.Type)

	byte_arr, err := json.Marshal(req)
	if err != nil {
		log.Println(err)
		return err
	}

	// call func directly instead of using RPC
	//service.TM_broadcast_tx_commit(byte_arr)
	str_hex := hex.EncodeToString(byte_arr)
	resultTx, err := core.BroadcastTxAsync([]byte(str_hex))


	log.Println("resultTx=", resultTx)

	if err != nil {
		log.Println(err.Error())
	}

	//*result = Rpc_Result { data: "sample data" }
	res.Msg = "OK"
	return nil
}



////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

//type BlockchainService struct {
//
//}
//
//type BlockchainService_ValSetChange_Request struct {
//	//Diffs 			[]tmtypes.Validator		// []*tmtypes.Validator
//	Diffs				[]BlockchainService_Validator
//}
//
//type BlockchainService_Validator struct {
//	PubKey		string
//	Power  		uint64
//}
//
//type BlockchainService_Response struct {
//	Msg 	string //interface{}
//}
//
//func (b *BlockchainService) ValSetChange(r *http.Request, req *BlockchainService_ValSetChange_Request, res *BlockchainService_Response) error {
//	for _, v := range req.Diffs {
//		log.Println("PubKey", v.PubKey, "Power", v.Power)
//
//
//		bPubKey, err := hex.DecodeString(v.PubKey)
//		if (err != nil) {
//			log.Println(err.Error())
//			res.Msg = err.Error()
//			return err
//		}
//
//		pubKey, err := types.GetPubKeyFromBytes(bPubKey)
//		if (err != nil) {
//			log.Println(err.Error())
//			res.Msg = err.Error()
//			return err
//		}
//
//		// NOTE: expects go-wire encoded pubkey
//		val_change := &tmtypes.Validator {
//			PubKey: pubKey.Bytes(),
//			Power: v.Power,
//		}
//
//
//		// TODO: need to validate if address of new validator existed or not
//		address := pubKey.Address() // address of validator
//		_, err = service.TreeGetAccount(app.PeerityGlobal.App.State, address)
//		if (err != nil) {
//			log.Println("Can not get validator address - ", err.Error())
//			res.Msg = err.Error()
//			return err
//		}
//
//		// all good!
//		app.PeerityGlobal.ValsChange = append(app.PeerityGlobal.ValsChange, val_change)
//	}
//
//	//// debug
//	//for _, v := range app.PeerityGlobal.ValsChange {
//	//	log.Println("DEBUGAAAAA", "PubKey", hex.EncodeToString(v.PubKey), "Power", v.Power)
//	//}
//
//	res.Msg = "OK"
//	return nil
//}

