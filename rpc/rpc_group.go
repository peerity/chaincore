package rpc

import (
	"net/http"
	"log"
	"encoding/hex"
	"strings"
	"gitlab.com/peerity/chaincore/service"
	"gitlab.com/peerity/chaincore/app"
)

type GroupService struct {

}

type GroupService_Request string // ID - hash of uppercase(Name)

type GroupService_Response struct {
	ID		string
	Name		string		// must be unique, so the Key is unique
	Description 	string		// type anything here
	Owner 		string		// address of account owner
}

func (t *GroupService) GetGroup(r *http.Request, req *GroupService_Request, res *GroupService_Response) error {
	ID := string(*req)
	log.Println("GetGroup", ID)

	ID_bytes, err := hex.DecodeString(ID)
	if (err != nil) {
		return err
	}

	group, err := service.TreeGetGroup(app.PeerityGlobal.App.State, ID_bytes)
	if (err != nil) {
		return err
	}

	res.ID = strings.ToUpper(ID)
	res.Name = group.Name
	res.Description = group.Description
	res.Owner = group.Owner

	return nil
}


