package rpc

import (
	"net/http"
	"log"
	"encoding/hex"
	"strings"
	"gitlab.com/peerity/chaincore/service"
	"gitlab.com/peerity/chaincore/app"
)

type PostService struct {

}

type PostService_Request string // ID of post

type PostService_Response struct {
	ID			string
	Title           	string
	Content         	string
	Author          	string
	Group			string
	//CreatedDate            	int64 		// unixtime
	//ModifiedDate        	int64 		// unixtime
	Image 		    	string 		// url to thumb image
	Cat 			[]string 	// categories
	Location 		string 		// related location
}

func (t *PostService) GetPost(r *http.Request, req *PostService_Request, res *PostService_Response) error {
	ID := string(*req)
	log.Println("GetPost", ID)

	ID_bytes, err := hex.DecodeString(ID)
	if (err != nil) {
		return err
	}

	post, err := service.TreeGetPost(app.PeerityGlobal.App.State, ID_bytes)
	if (err != nil) {
		return err
	}

	res.ID = strings.ToUpper(ID)
	res.Title = post.Title
	res.Content = post.Content
	res.Author = post.Author
	res.Group = strings.ToUpper(hex.EncodeToString(post.Group))
	//res.CreatedDate = post.CreatedDate
	//res.ModifiedDate = post.ModifiedDate
	res.Image = post.Image
	res.Cat = post.Cat
	res.Location = post.Location

	return nil
}


