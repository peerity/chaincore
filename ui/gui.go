package ui;

import (
	"github.com/gizak/termui"
	"gitlab.com/peerity/chaincore/debug"
)

func CreateLogo() *termui.Par {
	par0:= termui.NewPar("[WELCOME TO....\n"+
		"    ____                 _ __\n"+
		"   / __ \\___  ___  _____(_) /___  __\n"+
		"  / /_/ / _ \\/ _ \\/ ___/ / __/ / / /\n"+
		" / ____/  __/  __/ /  / / /_/ /_/ / \n"+
		"/_/    \\___/\\___/_/  /_/\\__/\\__, /  \n"+
		"                           /____/   \n"+
		"Connect. Create. Contribute](fg-green,fg-bold)");
	return par0;
}
var LogoSplash = CreateLogo();
var LogStream = termui.NewPar("loading");

var CurrentBlock = termui.NewPar("loading");
var BlockSpeed = termui.NewPar("--");
var NodesReadout  = termui.NewPar("We are the facilitators of our own creative evolution.")



func EnableDebug() {
	LogStream.Text = debug.DebugEnabledMessage;
}

func CreateInterface() {
	defer DisableGraphics();
	defer termui.Close()

	EnableGraphics();


	err := termui.Init()
	if err != nil {
		panic(err)
	}


	LogoSplash.Width = 100;
	LogoSplash.Height = 12;
	LogoSplash.Border=false;


	LogStream.Height = 5
	LogStream.Width = 37
	LogStream.Y = 4
	LogStream.BorderLabel = "Log Stream"
	LogStream.BorderFg = termui.ColorGreen


	CurrentBlock.Height = 3
	CurrentBlock.BorderLabel = "Current Block"

	quitMessage := termui.NewPar("[Press \"q\" to exit UI.](fg-red) ");
	if(debug.DebugEnabled) {
		quitMessage.Text = quitMessage.Text + debug.DebugEnabledMessage;
	}
	quitMessage.Height=3;
	quitMessage.Border=false;



	NodesReadout.Height=3;
	NodesReadout.BorderLabel="Bill Hicks says..."

	BlockSpeed.Height=3;
	BlockSpeed.BorderLabel="Block Speed"



	termui.Body.AddRows(
		termui.NewRow(
			termui.NewCol(12, 3, LogoSplash)),
		termui.NewRow(
			termui.NewCol(12, 0, NodesReadout)),
		termui.NewRow(
			termui.NewCol(6, 0, BlockSpeed),
			termui.NewCol(6, 0, CurrentBlock)),
		termui.NewRow(
			termui.NewCol(12, 0, LogStream)),
		termui.NewRow(
			termui.NewCol(12, 0, quitMessage)));

	// calculate layout
	termui.Body.Align()



	termui.Handle("/sys/kbd/q", func(termui.Event) {
		termui.StopLoop()
		Log("Disabling UI");
	})

	termui.Handle("/sys/kbd/C-c", func(termui.Event) {
		termui.StopLoop();
		Log("Disabling UI, quitting.");
	})

	termui.Handle("/sys/wnd/resize", func(e termui.Event) {
		termui.Body.Width = termui.TermWidth()
		termui.Body.Align()
		termui.Clear()
		termui.Render(termui.Body)

	})
	termui.Render(termui.Body)

	termui.Loop()

}