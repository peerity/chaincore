package ui

import (
	"fmt"
	"log"
	"time"
	//log "github.com/Sirupsen/logrus"
	"io/ioutil"
	"os"
	"github.com/tendermint/log15"
	"github.com/tendermint/go-logger"
	"github.com/gizak/termui"
)

var hicks = 0;
var currentQuote = 0;

var graphicsEnabled = false;
var hicksQuotes = []string{
	"We are the facilitators of our own creative evolution.",
	"We all pay for life with death, so everything in between should be free.",
	"I'm totally confused about what I'm going to do with my life.",
	"I left in love, in laughter, and in truth, and wherever truth, love and laughter abide, I am there in spirit.",
	"Life is only a dream and we are the imagination of ourselves",
	"I don't do drugs anymore... than, say, the average touring funk band.",
	"I get a kick out of being an outsider constantly. It allows me to be creative.",
	"Listen, the next revolution is gonna be a revolution of ideas."}



func Log(msg ...interface{}) {
	if(graphicsEnabled) {
		UIMessage(fmt.Sprint(msg...));
	} else {
		log.Output(2, fmt.Sprintln(msg...));
	}
}

func Fmt(msg ...interface{}) {
	if(graphicsEnabled) {
		UIMessage(fmt.Sprint(msg...));
	}else {
		fmt.Println(msg);
	}
}

func IsEnabled() bool {
	return graphicsEnabled;
}

var lasttime = time.Now();

func UpdateLastBlock() {
	if(graphicsEnabled) {
		now := time.Now();
		BlockSpeed.Text = fmt.Sprint(now.Sub(lasttime).Seconds()) + "s";
		lasttime = now;
		termui.Render(termui.Body);
	}
}

func UIMessage(message string) {
	if (graphicsEnabled) {
		UpdateHicks();
		LogStream.Text = message;
		termui.Render(termui.Body);
	}
}

func UpdateBlockHeight(blockheight uint64) {
	if(graphicsEnabled) {
		CurrentBlock.Text = fmt.Sprint(blockheight);
		termui.Render(termui.Body);
	}
}

func UpdateHicks() {
	if(graphicsEnabled) {
		hicks++;
		if(hicks>30) {
			hicks=0;
			currentQuote++;
			if(currentQuote>=cap(hicksQuotes)) {
				currentQuote=0;
			}
			NodesReadout.Text = fmt.Sprint(hicksQuotes[currentQuote]);
		}
	}
}
func UpdateNodeCount(count int) {
	//NodesReadout.Text = fmt.Sprint(count);
	//termui.Render(termui.Body);
}

func UpdateValidatorCount(count int) {

}


func EnableGraphics() {
	log.SetOutput(ioutil.Discard);
	graphicsEnabled = true;
	log15.Root().SetHandler(log15.LvlFilterHandler(
		logger.LvlInfo,
		log15.StreamHandler(ioutil.Discard, log15.TerminalFormat()),
	))
}

func DisableGraphics() {
	log.SetOutput(os.Stderr);
	graphicsEnabled = false;
	log15.Root().SetHandler(logger.MainHandler());
}

func Fatal(msg ...interface{}) {
	termui.StopLoop();
}
