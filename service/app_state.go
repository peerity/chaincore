package service

import (
	"bytes"
	dbm "github.com/tendermint/go-db"
	"github.com/tendermint/go-wire"
	. "github.com/tendermint/go-common"
	"encoding/hex"
	"github.com/tendermint/go-merkle"
	"gitlab.com/peerity/chaincore/types"
	"errors"
	//"golang.org/x/crypto/ripemd160"
	"gitlab.com/peerity/chaincore/ui"
)

//-----------------------------------------
// persist the last block info

const lastBlockKey = "lastblock"

type LastBlockInfo struct {
	Height  	uint64
	AppHash 	[]byte
	TxCount 	uint64
}

// Get the last block from the db
func LoadLastBlock(db dbm.DB) (lastBlock LastBlockInfo) {
	buf := db.Get([]byte(lastBlockKey))
	if len(buf) != 0 {
		r, n, err := bytes.NewReader(buf), new(int), new(error)
		wire.ReadBinaryPtr(&lastBlock, r, 0, n, err)
		if *err != nil {
			// DATA HAS BEEN CORRUPTED OR THE SPEC HAS CHANGED
			Exit(Fmt("Data has been corrupted or its spec has changed: %v\n", *err))
		}
		// TODO: ensure that buf is completely read.
	}

	return lastBlock
}

func SaveLastBlock(db dbm.DB, lastBlock LastBlockInfo) {
	ui.Log("Saving block ", "height = ", lastBlock.Height, " root = ", hex.EncodeToString(lastBlock.AppHash))
	buf, n, err := new(bytes.Buffer), new(int), new(error)
	wire.WriteBinary(lastBlock, buf, n, err)
	if *err != nil {
		// TODO
		PanicCrisis(*err)
	}
	db.Set([]byte(lastBlockKey), buf.Bytes())
}

////////////////////////////////
// Account

func TreeSaveAccount(state merkle.Tree, acc types.Account) error  {
	raw, err := types.StructToBytes(acc)
	if err != nil {
		ui.Log(err.Error())
		return err
	}

	state.Set(acc.AccountKey(), raw)

	return nil
}

func TreeGetAccount(state merkle.Tree, username string) (acc types.Account, err error) {
	var value []byte
	var exists bool

	accountKey := types.AccountKey(username)

	// get account
	_, value, exists = state.Get(accountKey)
	if (!exists) {
		err = errors.New("no account found")
		return
	}

	r, n := bytes.NewReader(value), new(int)
	wire.ReadBinaryPtr(&acc, r, 0, n, &err)
	if (err != nil) {
		return
	}

	return acc, err
}

////////////////////////////////
// Post

func TreeSavePost(state merkle.Tree, post types.Post) error  {
	raw, err := types.StructToBytes(post)
	if err != nil {
		ui.Log(err.Error())
		return err
	}

	state.Set(post.PostKey(), raw)

	return nil
}

func TreeGetPost(state merkle.Tree, key []byte) (post types.Post, err error) {
	var value []byte
	var exists bool

	postKey := types.PostKey(key)

	// get post
	_, value, exists = state.Get(postKey)
	if (!exists) {
		err = errors.New("no post found")
		return
	}

	r, n := bytes.NewReader(value), new(int)
	wire.ReadBinaryPtr(&post, r, 0, n, &err)
	if (err != nil) {
		return
	}

	return post, err
}

////////////////////////////////
// Comment

func TreeSaveComment(state merkle.Tree, comment types.Comment) error  {
	raw, err := types.StructToBytes(comment)
	if err != nil {
		ui.Log(err.Error())
		return err
	}

	state.Set(comment.CommentKey(), raw)

	return nil
}

func TreeGetComment(state merkle.Tree, key []byte) (comment types.Comment, err error) {
	var value []byte
	var exists bool

	commentKey := types.CommentKey(key)

	// get comment
	_, value, exists = state.Get(commentKey)
	if (!exists) {
		err = errors.New("no comment found")
		return
	}

	r, n := bytes.NewReader(value), new(int)
	wire.ReadBinaryPtr(&comment, r, 0, n, &err)
	if (err != nil) {
		return
	}

	return comment, err
}

////////////////////////////////
// Group

func TreeSaveGroup(state merkle.Tree, grp types.Group) error  {
	raw, err := types.StructToBytes(grp)
	if err != nil {
		ui.Log(err.Error())
		return err
	}

	state.Set(grp.GroupKey(), raw)

	return nil
}

func TreeGetGroup(state merkle.Tree, key []byte) (grp types.Group, err error) {
	var value []byte
	var exists bool

	groupKey := types.GroupKey(key)

	// get group
	_, value, exists = state.Get(groupKey)
	if (!exists) {
		err = errors.New("no group found")
		return
	}

	r, n := bytes.NewReader(value), new(int)
	wire.ReadBinaryPtr(&grp, r, 0, n, &err)
	if (err != nil) {
		return
	}

	return grp, err
}

////////////////////////////////
// Proposal

func TreeSaveProposal(state merkle.Tree, proposal types.Proposal) error  {
	raw, err := types.StructToBytes(proposal)
	if err != nil {
		ui.Log(err.Error())
		return err
	}

	state.Set(proposal.ProposalKey(), raw)

	return nil
}

func TreeGetProposal(state merkle.Tree, key []byte) (proposal types.Proposal, err error) {
	var value []byte
	var exists bool

	proposalKey := types.ProposalKey(key)

	// get proposal
	_, value, exists = state.Get(proposalKey)
	if (!exists) {
		err = errors.New("no proposal found")
		return
	}

	r, n := bytes.NewReader(value), new(int)
	wire.ReadBinaryPtr(&proposal, r, 0, n, &err)
	if (err != nil) {
		return
	}

	return proposal, err
}

//////////////////////

type SendTokenStateCommand struct {
	From		string 	// sender Username
	To	 	string 	// receiver Username
	Amount 		int64 	// how much
}

func Exec_SendToken(state merkle.Tree, cmd SendTokenStateCommand) error {
	sender, err := TreeGetAccount(state, cmd.From)
	if (err != nil) {
		return err
	}

	receiver, err := TreeGetAccount(state, cmd.To)
	if (err != nil) {
		return err
	}

	if (cmd.Amount <= 0) {
		return errors.New("Amout value is invalid")
	}

	if (sender.Balance < cmd.Amount) {
		return errors.New("Amout value is invalid")
	}

	// TODO: more validation
	sender.Balance -= cmd.Amount
	receiver.Balance += cmd.Amount

	sender.Sequence++
	// TODO: remove receiver.Sequence++ on next release, invalid app hash, need softfork or resetting network
	receiver.Sequence++

	TreeSaveAccount(state, sender)
	TreeSaveAccount(state, receiver)

	return nil // everything OK, return nil
}

func Exec_CreateGroup(state merkle.Tree, cmd types.GroupCreateOperation) error {
	group := types.Group {
		Name: cmd.Name,
		Description: cmd.Description,
		Owner: cmd.Owner }

	TreeSaveGroup(state, group)

	return nil // everything OK, return nil
}
