# chaincore
* blockchain core with rpc
* using tendermint blockchain framework
* ed25519 ( vs secp256k1 ?)

# rpc
* account
* crypto currency

# config
There are 2 configuration for **Tendermint** and **Chaincore** App
* Chaincore App config file is `config.json`, rename the `config-sample.json` to `config.json` and change it.
* Tendermint config contains several files, see [Getting started](https://gitlab.com/peerity/chaincore/wikis/getting-started)

