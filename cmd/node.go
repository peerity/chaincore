// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (

	"os"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/tendermint/go-logger"
	"github.com/tendermint/tendermint/node"
	tmtypes "github.com/tendermint/tendermint/types"
	"flag"
	. "github.com/tendermint/go-common"
	"gitlab.com/peerity/chaincore/rpc"
	. "gitlab.com/peerity/chaincore/app"
	"log"
	cfg "github.com/tendermint/go-config"
	tmcfg "github.com/tendermint/tendermint/config/tendermint"
	"time"
	"gitlab.com/peerity/chaincore/types"
	"io/ioutil"
	"github.com/tendermint/go-p2p"
	"strings"
	"gitlab.com/peerity/chaincore/ui"
	"github.com/tendermint/log15"
	"github.com/gizak/termui"
	_ "net/http/pprof"
	"net/http"
	"gitlab.com/peerity/chaincore/debug"
	"fmt"
	tmnode "github.com/tendermint/tendermint/node"
	tmproxy "github.com/tendermint/tendermint/proxy"
	"github.com/tendermint/abci/server"
)

//global debug val.
var DebugEnabled bool;
var cfgFile string;
var headless bool;
var DebugServerAddress ="localhost:6060";
var debugMessage = "Debug pprof server enabled at " +DebugServerAddress+"";
var WelcomeMessage = "Preparring to launch Peerity...";

// nodeCmd represents the node command
var nodeCmd = &cobra.Command{
	Use:   "node",
	Short: "Starts peerity node",
	Long: `Starts a peerity node using the available config files.  If config files don't exist try running the "setup" command.""'.`,
	Run: func(cmd *cobra.Command, args []string) {
		//debug and profiling.
		if(DebugEnabled) {
			ui.EnableDebug();
			os.Stdout.WriteString("Debug enabled\n\n\n");
			go http.ListenAndServe(debug.DebugServerAddress, nil);
		}
		if(!headless) {
			go ui.CreateInterface();

		}else {
			ui.DisableGraphics();
		}

		launch();
	},
}


/**
 Start Tendermint service as embedded mode.
 - Simpler for deploying
 - Hopefully avoid the panic bug when stop Glogchain before TM.

 TODO: look at TM in-proc app to see if there is any imrovment on glogchain ( https://github.com/tendermint/basecoin/blob/master/cmd/commands/start.go#L88 )
 */
func init() {
	RootCmd.AddCommand(nodeCmd)
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags, which, if defined here,
	// will be global for your application.

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.chaincore.yaml)")
	RootCmd.PersistentFlags().BoolVarP(&debug.DebugEnabled, "debug", "d", false, "Enable Golang Debug Server")
	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	RootCmd.PersistentFlags().BoolVarP(&headless, "headless","", false, "Run without UI, just logs");
	RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	// to change the flags on the default logger
	log.SetFlags(log.LstdFlags | log.Lshortfile)


	// create tmp folder if need
	_, err := os.Stat("./tmp")
	if os.IsNotExist(err) {
		os.Mkdir("./tmp", os.ModePerm)
	}

}


// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName(".chaincore") // name of config file (without extension)
	viper.AddConfigPath(os.Getenv("HOME")) // adding home directory as first search path
	viper.AutomaticEnv()                   // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		ui.Fmt("Using config file:", viper.ConfigFileUsed())
	}
}


var tm_config cfg.Config

func launch() {
	// Read Peerity config (config.json)
	types.ReadConfig()
	if(ui.IsEnabled()) {
		log.SetOutput(ioutil.Discard);
	}
	// Get TM configuration
	tm_config = tmcfg.GetConfig(types.PeerityConfigGlobal.TmRoot)

	// fetch some config from TM (config.toml) to Peerity (config.json)
	types.PeerityConfigGlobal.TmRpcLaddr = strings.Replace(types.PeerityConfigGlobal.TmRpcLaddr, "tcp://", "http://", 1)

	/////////////////////////////////////////////
	flag.Parse()

	PeerityGlobal.App = NewPeerityApp()

	//ui.Log("PeerityGlobal.App.Height=", PeerityGlobal.App.Height)

	// add account from genesis.json dynamically
	//if (PeerityGlobal.App.Height == 0) { // genesis block test
	//	PeerityGlobal.App.SetOption("genesis.block/create.account", "pool/449F6F39391BC9E918CE51DB10F6FAADF65077263E715B17652425CD7827C814/1000000")
	//	PeerityGlobal.App.SetOption("genesis.block/create.account", "jan/CDD6774218138DF657C7B0494894BBA596EB2ECCCC4C946D5ACEF3B5FCD2CE7B/1000")
	//	PeerityGlobal.App.SetOption("genesis.block/create.account", "tuan/EB3B42091EF6C2F8FA951319940C003BEC7AAE2336BD2AFABD6FB59EB4A3EF6E/1000")
	//}

	/////////////////////////////////////////////
	// Start the listener
	s, err := server.NewServer(tm_config.GetString("proxy_app"), "grpc", PeerityGlobal.App)
	if err != nil {
		Exit(err.Error())
	}

	writeSplashScreen();

	/////////////////////////////////////////////
	// start json rpc server on port 8088
	go rpc.StartRpcServer()

	/////////////////////////////////////////////
	// start web server on port 8000
	//go web.StartWebServer()

	/////////////////////////////////////////////
	// start embedded tendermint
	go startTendermintNode()
	//go userinterface();
	//// Wait forever
	TrapSignal(func() {
		termui.StopLoop();
		//tm_node.Stop()
		s.Stop()
	})
}

func writeSplashScreen() {
	if(!ui.IsEnabled()) {

		fmt.Println("****************************************");
		fmt.Println("WELCOME TO....");
		fmt.Println("    ____                 _ __")
		fmt.Println("   / __ \\___  ___  _____(_) /___  __");
		fmt.Println("  / /_/ / _ \\/ _ \\/ ___/ / __/ / / /");
		fmt.Println(" / ____/  __/  __/ /  / / /_/ /_/ / ");
		fmt.Println("/_/    \\___/\\___/_/  /_/\\__/\\__, /  ");
		fmt.Println("                           /____/   ");
		fmt.Println("Connect. Create. Contribute.");
		fmt.Println("****************************************");
	}
}


func startTendermintNode()  {
	// set the log level
	logger.SetLogLevel(tm_config.GetString("log_level"))

	//when disabled this seems to go away.  Which is kinda odd.
	if(ui.IsEnabled()) {
		//logger.SetLogLevel("error");
		log15.Root().SetHandler(log15.LvlFilterHandler(
			logger.LvlError,
			log15.StreamHandler(ioutil.Discard, log15.TerminalFormat()),
		))
	}

	// wait sometime to make sure glogchain is up
	if(DebugEnabled ){
		ui.Fmt(WelcomeMessage, "\n", debugMessage);
	} else {
		ui.Fmt(WelcomeMessage)
	}
	time.Sleep(time.Second * 5)
	RunNode(tm_config);
}


// Users wishing to:
//	* use an external signer for their validators
//	* supply an in-proc abci app
// should fork tendermint/tendermint and implement RunNode to
// call NewNode with their custom priv validator and/or custom
// proxy.ClientCreator interface
func RunNode(config cfg.Config) {
	// Wait until the genesis doc becomes available
	genDocFile := config.GetString("genesis_file")
	if !FileExists(genDocFile) {
		panic("No genesis file")
	}

	jsonBlob, err := ioutil.ReadFile(genDocFile)
	if err != nil {
		Exit(Fmt("Couldn't read GenesisDoc file: %v", err))
	}
	genDoc := tmtypes.GenesisDocFromJSON(jsonBlob)
	if genDoc.ChainID == "" {
		PanicSanity(Fmt("Genesis doc %v must include non-empty chain_id", genDocFile))
	}
	config.Set("chain_id", genDoc.ChainID)

	////////////////////////////////////////////////////////////////////////////////////
	// add accounts from genesis.json dynamically
	if (PeerityGlobal.App.Height == 0) {
		for _, validator := range genDoc.Validators {
			strOption := fmt.Sprintf("%s/%s/%d", validator.Name, validator.PubKey.KeyString(), validator.Amount)
			log.Println("strOption=" + strOption)
			PeerityGlobal.App.SetOption("genesis.block/create.account", strOption)
		}
	}

	// Create & start node
	//n := NewNodeInProcess(config)
	privValidatorFile := config.GetString("priv_validator_file")
	privValidator := tmtypes.LoadOrGenPrivValidator(privValidatorFile)
	//n := tmnode.NewNode(config, privValidator, tmproxy.NewLocalClientCreator(PeerityGlobal.App))

	n := tmnode.NewNode(config, privValidator, tmproxy.DefaultClientCreator(config))


	protocol, address := node.ProtocolAndAddress(config.GetString("node_laddr"))
	l := p2p.NewDefaultListener(protocol, address, config.GetBool("skip_upnp"))
	n.AddListener(l)

	err = n.Start()
	if err != nil {
		Exit(Fmt("Failed to start node: %v", err))
	}

	//ui.Log("Started node", "nodeInfo", n.sw.NodeInfo())
	ui.Log("Started node", "nodeInfo", n.NodeInfo())


	// If seedNode is provided by config, dial out.
	if config.GetString("seeds") != "" {
		seeds := strings.Split(config.GetString("seeds"), ",")
		n.DialSeeds(seeds)
	}

	// Run the RPC server.
	if config.GetString("rpc_laddr") != "" {
		_, err := n.StartRPC()
		if err != nil {
			PanicCrisis(err)
		}
	}

	PeerityGlobal.Node = n

	// Sleep forever and then...
	TrapSignal(func() {
		n.Stop()
	})
}
