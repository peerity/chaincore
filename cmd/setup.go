// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"github.com/spf13/cobra"
	"io/ioutil"
	"os"
	"net/http"
	"time"
	"path/filepath"
	"encoding/json"
	"math/rand"
	"github.com/BurntSushi/toml"
	"gitlab.com/peerity/chaincore/types"
	"io"
	"gitlab.com/peerity/chaincore/ui"
)

const ConfigJsonFinalLocation = "./config.json";
const ConfigTomlFinalLocation = "./data/tendermint";

//const SettingsUrl = "https://raw.githubusercontent.com/Peerity/settings/master/ServerConfig.json";
const SettingsUrl = "https://raw.githubusercontent.com/baabeetaa/settings/patch-1/ServerConfig.json"
const GenesisUrl = "https://raw.githubusercontent.com/baabeetaa/settings/patch-1/genesis.json"
const ConfigJsonUrl = "https://raw.githubusercontent.com/baabeetaa/settings/patch-1/config.json"
const ConfigTomUrl = "https://raw.githubusercontent.com/baabeetaa/settings/patch-1/config.toml"


type SetupData struct {
	Json types.PeerityCfg
	Toml types.BlockchainSettings
}

// setupCmd represents the setup command
var setupCmd = &cobra.Command{
	Use:   "setup",
	Short: "Creates configs",
	Long: "Creates configuration files for Peerity. Uses helpful defaults without flags.",
	Run: func(cmd *cobra.Command, args []string) {
		// TODO: Work your own magic here
		ui.Fmt("setup called")

		//CreateConfigFiles();
		// Download files instead

		os.MkdirAll(ConfigTomlFinalLocation, os.ModePerm);

		// download config.json file
		configjson_filename, _ := filepath.Abs(ConfigJsonFinalLocation);
		err := downloadFile(configjson_filename, ConfigJsonUrl)
		if (err != nil) {
			ui.Log("Can not download config.json file from internet. Please check your internet connection and try again.")
			return
		}

		// download genesis file
		genesis_filename, _ := filepath.Abs(ConfigTomlFinalLocation+"/genesis.json");
		err = downloadFile(genesis_filename, GenesisUrl)
		if (err != nil) {
			ui.Log("Can not download genesis.json file from internet. Please check your internet connection and try again.")
			return
		}

		// download config.toml file
		configtoml_filename, _ := filepath.Abs(ConfigTomlFinalLocation + "/config.toml");
		err = downloadFile(configtoml_filename, ConfigTomUrl)
		if (err != nil) {
			ui.Log("Can not download config.toml file from internet. Please check your internet connection and try again.")
			return
		}
	},
}



func init() {
	RootCmd.AddCommand(setupCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// setupCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// setupCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}

func createConfigJson(settings *SetupData) {
	filename, _ := filepath.Abs(ConfigJsonFinalLocation)
	ui.Fmt("filename: %s\n", filename)

	_, rerr := ioutil.ReadFile(filename)
	//detect if file exists.  If so, do nothing.
	if rerr == nil {
		ui.Log("Config json exists, skipping");
	} else {
		ui.Log("Attempting to create config.json...");
		configbytes, err := json.Marshal(settings.Json);
		if(err!=nil) {
			werr := ioutil.WriteFile(ConfigJsonFinalLocation, configbytes, 0644)
			if werr != nil {
				ui.Fmt(werr)
				return
			}
			ui.Log("config.json created successfully.")
		} else {
			ui.Log("Unable to convert data for config.json");
		}
	}
}

func createConfigToml(settings *SetupData) {
	os.MkdirAll(ConfigTomlFinalLocation, os.ModePerm);
	filename, _ := filepath.Abs(ConfigTomlFinalLocation+"/config.toml");
	_, terr := ioutil.ReadFile(filename);
	if(terr==nil) {
		ui.Log("Config toml exist, skipping");
	} else {
		settings.Toml.Moniker = buildConfigToml();
		tomlmarshaler := toml.TextMarshaler(settings.Toml)
		tomlbytes, err := tomlmarshaler.MarshalText();

		if(err==nil) {
			ui.Log(string(tomlbytes));
			wterr := ioutil.WriteFile(ConfigTomlFinalLocation+"/config.toml", tomlbytes, 0644);
			if (wterr != nil) {
				ui.Fmt(wterr);
				return;
			}
			ui.Log("config.toml created successfully");

		} else {
			ui.Fmt(err);
			ui.Log("Unable to convert TOML data for config.toml")
		}
	}
}


func CreateConfigFiles() {
	resp, err := http.Get(SettingsUrl);
	if(err!=nil) {

	}
	defer resp.Body.Close();
	var settings = &SetupData{};

	if (resp.StatusCode == 200 && err==nil) { // OK
		bodyBytes, _ := ioutil.ReadAll(resp.Body);
		json.Unmarshal(bodyBytes, settings)

	} else {
		ui.Log("Could not get settings from  "+SettingsUrl);
		ui.Log("Check internet access and try again.")
		return;
	}

	createConfigJson(settings);
	createConfigToml(settings);
	return;
}


var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func buildConfigToml() string {
	hostname, err := os.Hostname();
	if(err!=nil) {
		ui.Log("cannot get hostname, generating pseudo-random name");
		rand.Seed(time.Now().UnixNano());
		hostname = "UNDEFINED-" + randSeq(10);
	}
	return hostname
}

func downloadGenesisFile() (err error)  {
	// Create the file
	genesis_filename, _ := filepath.Abs(ConfigTomlFinalLocation+"/genesis.json");
	out, err := os.Create(genesis_filename)
	if err != nil  {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(GenesisUrl)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil  {
		return err
	}

	return nil
}

func downloadFile(filepath string, url string) (err error) {

	// Create the file
	out, err := os.Create(filepath)
	if err != nil  {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil  {
		return err
	}

	return nil
}